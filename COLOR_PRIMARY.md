# Primary Color Palette

## Main Color Palette

Used consistently, our primary palette delivers strong brand recognition across all expressions and provide clear navigation through our products and services.


### White
RGB: `RGB(255,255,255)`

HEX: `#FFFFFF`

> The use of White space makes our material clear and straightforward.

### Cyan
RGB: `RGB(5,155,253)`

HEX: `#059BFD`
> The single, consistent use of vibrant Cyan builds familiarity.

### Purple
RGB: `RGB(82,36,160)`

HEX: `#5224A0`
> Purple is used to add brightness to text and graphics.

### Dark Purple
RGB: `RGB(63,25,98)`

HEX: `#3F1962`
> Dark purple is our neutral color. Its used for supporting type and body copy.

### Silver
RGB: `RGB(132,135,137)`

HEX: `#848789`
> Silver is used for supporting copy.

### Black
RGB: `RGB(0,0,0)`

HEX: `#000000`
> Black is used for single color production.


## Functional Digital Palette

As well as our primary and [secondary](COLOR_SECONDARY.md) color palette, there are some additional colors we use for digital. The digital palette has been developed purely for functional use. 

### Blue
RGB: `RGB(8,93,169)`

HEX: `#085DA9`
> Blue is used for buttons where call to action is set in white and for navigational elements.

### Green
RGB: `RGB(114,191,68)`

HEX: `#72BF44`
> Green is used for confirmation messages.

### Red
RGB: `RGB(224,48,48)`

HEX: `#E03030`
> Red is used for warning messages.

### Copy grey
RGB: `RGB(64,64,64)`

HEX: `#444444`
> Copy grey is used for body copy.

### Mid grey
RGB: `RGB(183,183,183)`

HEX: `#B7B7B7`
> Mild gray is used for shadows, strokes and dividers.

### Light grey
RGB: `RGB(241,241,241)`

HEX: `#F1F1F1`
> Light grey is used for backgrounds on boxes, e.g. notes.
