# Secondary Color Palette
Secondary colors should always support the [primary pallete](COLOR_PRIMARY.md) and should never be used as `hero` colors.

### Yellow
RGB: `RGB(233,179,25)`

HEX: `#E9B319`
> Yellow is used as accent color for the activation mark, infographics, backgrounds, charts and tables.

### Raspberry
RGB: `RGB(154,3,85)`

HEX: `#9A0355`
> Raspberry is used as accent color for text, backgrounds, holding shape, infographics, charts and tables.

### Turquoise
RGB: `RGB(111,180,170)`

HEX: `#6FB4AA`
> Turquoise, similar to raspberry, is used as accent color for text, backgrounds, holding shape, infographics, charts and tables.

### Blackcurrant
RGB: `RGB(100,39,105)`

HEX: `#642769`
> Blackcurrant, similar to raspberry, is used as accent color for text, backgrounds, holding shape, infographics, charts and tables.

