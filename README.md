# Branding for `IDBE LABS LTD`

This repository will contain all artefacts related to branding of the company.

- [Primary Color Palette](COLOR_PRIMARY.md)
- [Functional Digital Color Palette](COLOR_PRIMARY.md)
- [Secondary Color Palette](COLOR_SECONDARY.md)